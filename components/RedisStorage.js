const config = require("../config/params.js");
const Redis = require('redis').createClient();

/**
 * Компонент для работы с хранилищем Redis. Представляет из себя обертку над
 * стандартными функциями библиотеки redis.
 */

module.exports = {

  /**
   * Получить полные данные из хранилища в качестве массива объектов
   * @param {Function} callback Функция обратного вызова
   * @return {Function}
   */
  getData (callback) {
    Redis.get(config.redis.storage, (err, result) => {
      var redisData = [];

      if (result) {
        redisData = JSON.parse(result);
      }

      return callback(redisData);
    })
  },

  /**
   * Вставить данные в коллекцию. В большистве случаев это обновление данных
   * @param {Array} Данные для записи
   */
  setData (data, callback) {
    Redis.set(config.redis.storage, JSON.stringify(data), () => {
      if (callback) {
        return callback();
      }
    });
  },

  /**
   * Получить конкретную запись по айдишнику
   * @param {String}    id          Айдишник записи
   * @param {Function}  callback    Функция обратного вызова
   * @param {Function}
   */
  getItem (id, callback) {
    this.getData((data) => {

      var index = null;
        for (let i = 0; i < data.length; i++) {
          if (id === data[i]._id) {
            index = i;
          }
        }
        callback(data[index]);
    })
  },

  /**
   * Обновить конкретную запись в хранилище
   * @param {Object} item Данные обновляемого объекта
   */
  setItem(id, item, callback) {
    this.getData((data) => {
      for (var i = 0; i < data.length; i++) {
        if (id === data[i]._id) {
          data[i] = item;
          this.setData(data, () => {
            if (callback) {
              callback();
            }
          });

        }
      }
    });
  },

  deleteItem(id, callback) {
    this.getData((data) => {

      for (var i = 0; i < data.length; i++) {
        if (id == data[i]._id) {
          data.splice(i, 1);
        }
      }

      this.setData(data, () => {
        if (callback) {
          callback();
        }
      })
    })
  },

  getTaskIds(callback) {
    var result = [];
    this.getData((data) => {
      data.forEach((item) => {
        result.push(item._id);
      })
      callback(result)
    })
  },

  insertItem(item, callback) {
    this.getData((data) => {
      data.push(item);
      this.setData(data, () => {
        callback();
      })
    })
  },

}
