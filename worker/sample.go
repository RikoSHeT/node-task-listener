package main

import (
    "io"
    "log"
    "net/http"
    "os"
    "math/rand"
    "time"
)
/**
 * Генерим рандомное целочисленное значение от min до max
 */
func random(min, max int) int {
    rand.Seed(time.Now().Unix())
    return rand.Intn(max - min) + min
}

/**
 * Собираем маршрут, по которому стучаться
 */
func getRoute(route string) string {
    //return "http://example.com/"
    return "http://localhost:3000/" + route
}

func main() {

    // Вычисляем, сколько спать. От одной до 15 секунд
    sleepTime := random(1, 15)

    // Вычисляем вероятность, куда постучимся
    probability := random(0, 100) > 50

    // Засыпаем на рандомное время
    time.Sleep(time.Duration(sleepTime * 1000) * time.Millisecond)

    var target string

    if probability {
        target = getRoute("save-report")
    } else {
        target = getRoute("save-error")
    }

    // Выполняем запрос
    response, err := http.Get(target)
    if err != nil {
        log.Fatal(err)
    }

    // Заботимся о том,чтобы соединение закрылось после выполнения запроса
    defer response.Body.Close()

    // Выводим в стандартный поток
    _, _ = io.Copy(os.Stdout, response.Body)

}
