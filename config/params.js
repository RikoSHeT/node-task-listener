module.exports = {
  period: 3, //sec
  timeout: 60, //sec
  maxErrors: 3,
  collections: {
    tasks: "tasks",
  },
  db: {
    name: "task-tracker",
    host: "localhost",
    port: 27017,
    getConnectionString () {
      return `mongodb://${this.host}:${this.port}/${this.name}`;
    },
  },
  redis: {
    channel: "task-tracker",
    storage: "pending-list",
    host: "localhost",
    port: 6379,
  },
  machines: {
      filename: "Vagrantfile",
      folder: "machines",
      source: "vagrant-config",
      max: 3,
  },
  init: {
    tasksCount: 3,
  },
};
