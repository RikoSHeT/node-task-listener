const express = require('express')();

// Экземпляр компонента нужен отдельно
const mongo = require('mongodb')

const MongoClient = mongo.MongoClient;

// Создаем двух клиентов. Один подписывается на канал, второй публикует в него
const RedisPublisher = require('redis').createClient();
const RedisSubscriber = require('redis').createClient();

// Объект-обертка, для удобного получения данных из хранилища Redis
const RedisStorage = require('./components/RedisStorage.js')

const config = require("./config/params.js");

// Основной класс, выполняющий работу с vagrant-машиной
var Routine = require("./components/Routine.js");

// Соединение с БД
var db;

// Счетчик запущенных машин
global.runnedCount = 0;

// Слушаем роуты
express.get('/save-report', function (req, res) {
  res.sendFile(__dirname + '/views/save-report.json');
});

express.get('/save-error', function (req, res) {
  res.sendFile(__dirname + '/views/save-error.json');
});

express.get('/', function (req, res) {
  res.send("ok");
});

express.listen(3000, function () {
  console.log('App listening on port 3000!');
});

// Создаем соединение с БД
MongoClient.connect(config.db.getConnectionString(), (err, connection) => {

  if (err) {
    throw err;
  }
  // БД нам нужна глобально
  db = connection;
});

// Слушаем канал...
RedisSubscriber.subscribe(config.redis.channel)

RedisSubscriber.on("message", (channel, message) => {

  setTimeout(() => {
  // Чтобы меньше нагружать проц, гоняем сообщения только 3 раза в секунду
  var item = JSON.parse(message);

    // Получаем коллекцию из Redis-хранилища
    RedisStorage.getItem(item._id, (task) => {

      if (task) {
        // Готовим экземпляр машины
        var instance = new Routine(task._id);

        // Назначаем слушателей на события
        instance.emitter.on("created", () => {
          task.start = timestamp();
          RedisStorage.setItem(task._id, task, () => {
            RedisPublisher.publish(config.redis.channel, JSON.stringify(task));
            instance.run();
          })
        });

        instance.emitter.on("ready", () => {
          instance.readResponse();
        });

        instance.emitter.on("read-response", () => {
          instance.put();
        })

        instance.emitter.on("putted", (item) => {
          RedisStorage.setItem(item._id, item, () => {
              RedisPublisher.publish(config.redis.channel, JSON.stringify(item));
          })
          instance.die();
        })

        // Проверяем, как поступим с сообщением
        switch (task.status) {

          // Если задача новая - запускаем в обработку
          case "none":

            // Запускаем только в том случае, если количество запущенных машин
            // не превышает заданного количества
            if (global.runnedCount < config.machines.max) {
              // Меняем статус
              task.status = "executing";
              task.start = 0;

              RedisStorage.setItem(task._id, task, () => {
                instance.init();
              });

            }

            // Пушим задачу обратно в канал
            //RedisPublisher.publish(config.redis.channel, JSON.stringify(task));
            break;

          // Если задача в процессе выполнения
          case "executing":

            /*var difference = timestamp() - task.start;

            if (task.start !== 0 && difference > config.timeout) {
              console.log(difference);
              task.status = "timeout";
            }*/

            RedisStorage.setItem(task._id, task, () => {
              RedisPublisher.publish(config.redis.channel, JSON.stringify(task));
            })
            break;

          // Если случилась ошибка выполнения...
          // Либо прога стукнулась в error, либо не смогла соединиться
          case "error":
            console.log(`Error: ${task._id}`);

            // Если это первая ошибка - задаем количество
            if (!task.errors) {
              task.errors = 0;
            }

            // Если ошибок не больше заданного количества
            if (task.errors < config.maxErrors) {
              // Инкрементируем счетчик ошибок задачи
              task.errors++;
              task.status = "none";

            } else {
              task.status = "broken";
            }

            RedisStorage.setItem(task._id, task, () => {
              RedisPublisher.publish(config.redis.channel, JSON.stringify(task));
              if (instance.machine) {
                instance.die();
              }
            })

            break;

          // В случае успеха задачи
          case "success":
            console.log(`Success: ${task._id}`);
            instance.die();
            (() => {
              let collection = db.collection(task.status);
              collection.insertOne(task);
            })();

            break;

          case "broken":
            console.log(`Broken!: ${task._id}`);
            instance.die();

            (() => {
              let collection = db.collection(task.status);
              collection.insertOne(task);
            })();

            break;

          // Если задача залипла
          /*case "timeout":
            console.log(`Timeout: ${task._id}`);
            instance.die();

            (() => {
              let collection = db.collection(task.status);
              collection.insertOne(task);
            })();

            break;*/
        }
      }
    });

  }, 300);

})

// Раз в заданное время заполняем очередь задач
setInterval(() => {

  // Если соединения нет - отправляемся на следующий тик
  if (db) {

    // Определяем коллекцию
    var collection = db.collection(config.collections.tasks);

    // Здесь храним айдишники, которые в выборке не нужны, то есть - которые уже
    // выполняются или выполнились

    RedisStorage.getTaskIds((excluded) => {

      for (var i = 0; i < excluded.length; i++) {
        excluded[i] = new mongo.ObjectID(excluded[i]);
      }

      collection.find({_id: {$nin: excluded}}).limit(1).toArray((err, selection) => {
        selection.forEach((item) => {
          RedisStorage.insertItem(item, () => {
            RedisPublisher.publish(config.redis.channel, JSON.stringify(item));
          })
        });
        collection.remove({id: {$in: excluded}});
      });
    });

  } else {
    console.log("Can't connect to database!");
  }
}, config.period * 1000)

/**
 * Получить текущую временную метку Unix
 * @return {int}
 */
function timestamp()
{
  return Math.floor(Date.now() / 1000);
}
